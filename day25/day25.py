def get_number(string):
    try:
        return int(string)
    except ValueError:
        return registers[string]


def check_signal():

    with open("input25.txt") as input_file:
        instructions = [line.strip().split() for line in input_file]

    start = 0
    output = ""
    while start < len(instructions):
        for i, instr in enumerate(instructions[start:]):
            pos = start + i
            command = instr[0]
            x = instr[1]
            if len(instr) == 3:
                y = instr[2]
            if command == "cpy":
                registers[y] = get_number(x)
            elif command == "inc":
                registers[x] += 1
            elif command == "dec":
                registers[x] -= 1
            elif command == "jnz":
                if get_number(x) != 0:
                    start = pos + get_number(y)
                else:
                    start = pos + 1
                break
            elif command == "tgl":
                index = pos + get_number(x)
                if index < len(instructions):
                    if instructions[index][2] is None:
                        if instructions[index][0] == "inc":
                            instructions[index][0] = "dec"
                        else:
                            instructions[index][0] = "inc"
                    else:
                        if instructions[index][0] == "jnz":
                            instructions[index][0] = "cpy"
                        else:
                            instructions[index][0] = "jnz"
            elif command == "out":
                output += str((get_number(x)))
                if len(output) == 10:
                    if output == "01" * 5 or output == "10" * 5:
                        return True
                    else:
                        return False
        else:
            return False


a = 0
while True:
    registers = {"a": a, "b": 0, "c": 0, "d": 0}
    if check_signal() is True:
        print("Lowest integer that produces a clock signal: {0}".format(a))
        break
    a += 1
