from collections import deque


def bfs(fields, root):
    reachable = 1
    root.distance = 0
    queue = deque([root])
    while len(queue) > 0:
        current = queue.popleft()
        adjacent_fields = [field for field in fields if abs(current.x - field.x) == 1 and abs(current.y - field.y) == 0
                           or abs(current.x - field.x) == 0 and abs(current.y - field.y) == 1]
        for field in adjacent_fields:
            if field.distance is None:
                field.distance = current.distance + 1
                queue.append(field)
                if field.x == 31 and field.y == 39:
                    print("Fewest number of steps to reach (31,39): {0}".format(field.distance))
                if field.distance <= 50:
                    reachable += 1
    print("Locations reachable with up to 50 steps: {0}".format(reachable))


class Field:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.distance = None


open_spaces = []
for x in range(52):
    for y in range(52):
        if x == 1 and y == 1:
            continue
        elif bin(x*x + 3*x + 2*x*y + y + y*y + 1350).count("1") % 2 == 0:
            open_spaces.append(Field(x, y))
bfs(open_spaces, Field(1, 1))
