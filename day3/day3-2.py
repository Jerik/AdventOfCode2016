valid_triangles = 0
three_lines = []
with open("input3.txt") as input_file:
    for i, line in enumerate(input_file):
        three_lines.append([int(side_length) for side_length in line.split("  ") if side_length != ""])
        if (i + 1) % 3 == 0:
            for j in range(3):
                triangle = [one_line[j] for one_line in three_lines]
                triangle.sort()
                if triangle[2] < triangle[0] + triangle[1]:
                    valid_triangles += 1
            three_lines.clear()
print("Number of valid triangles: {0}".format(valid_triangles))
