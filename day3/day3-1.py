valid_triangles = 0
with open("input3.txt") as input_file:
    for line in input_file:
        triangle = [int(side_length) for side_length in line.split("  ") if side_length != ""]
        triangle.sort()
        if triangle[2] < triangle[0] + triangle[1]:
            valid_triangles += 1
print("Number of valid triangles: {0}".format(valid_triangles))
