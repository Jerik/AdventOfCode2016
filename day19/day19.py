num_of_elves = 3005290
winner1 = bin(num_of_elves)[3:] + bin(num_of_elves)[2]
print("Winning elf (part 1): {0}".format(int(winner1, 2)))
winner2 = 0
for i in range(num_of_elves):
    if 3**i >= num_of_elves:
        winner2 = 3**i
        for j in range(3**i - num_of_elves):
            if j < 3**(i-1):
                winner2 -= 2
            else:
                winner2 -= 1
        break
print("Winning elf (part 2): {0}".format(winner2))
