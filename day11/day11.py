import re
from collections import defaultdict, deque
from itertools import combinations


def bfs(init):
    visited_states = defaultdict(bool)
    visited_states[(init.elevator, init.get_pairs())] = True
    init.distance = 0
    queue = deque([init])
    while len(queue) > 0:
        current = queue.popleft()
        for child_state in current.get_child_states():
            if not child_state.test_fried() and not visited_states[(child_state.elevator, child_state.get_pairs())]:
                child_state.distance = current.distance + 1
                queue.append(child_state)
                visited_states[(child_state.elevator, child_state.get_pairs())] = True
                if all([len(floor) == 0 for floor in child_state.floors[:3]]):
                    print(child_state.distance)
                    queue.clear()
                    break


class State:

    def __init__(self, elevator, floors):
        self.elevator = elevator
        self.floors = floors
        self.distance = None

    def test_fried(self):
        for floor in self.floors:
            chips = [item for item in floor if item.type == "chip"]
            gens = [item for item in floor if item.type == "generator"]
            for chip in chips:
                if len(gens) > 0 and all([gen.element != chip.element for gen in gens]):
                    return True
        return False

    def get_child_states(self):
        child_states = []
        for item in self.floors[self.elevator]:
            if 0 <= self.elevator - 1 <= 2:
                child_states.append(self.get_child_state(-1, item))
            if 1 <= self.elevator + 1 <= 3:
                child_states.append(self.get_child_state(1, item))
        for comb in combinations(self.floors[self.elevator], 2):
            if 0 <= self.elevator - 1 <= 2:
                child_states.append(self.get_child_state(-1, comb[0], comb[1]))
            if 1 <= self.elevator + 1 <= 3:
                child_states.append(self.get_child_state(1, comb[0], comb[1]))
        return child_states

    def get_child_state(self, floor_diff, *items):
        child_floors = [list(floor) for floor in self.floors]
        for item in items:
            child_floors[self.elevator + floor_diff].append(item)
            child_floors[self.elevator].remove(item)
        return State(self.elevator + floor_diff, child_floors)

    def get_pairs(self):
        pairs = []
        item_to_floor_index = {}
        for i, floor in enumerate(self.floors):
            for item in floor:
                item_to_floor_index[item] = i
        chips = [item for item in item_to_floor_index if item.type == "chip"]
        gens = [item for item in item_to_floor_index if item.type == "generator"]
        for chip in chips:
            for gen in gens:
                if chip.element == gen.element:
                    pairs.append((item_to_floor_index[chip], item_to_floor_index[gen]))
                    gens.remove(gen)
        return tuple(sorted(pairs))


class Item:

    def __init__(self, item_type, element):
        self.type = item_type
        self.element = element


floors = []
with open("input11.txt") as input_file:
    for line in input_file:
        floor = []
        for element in re.findall(r"([a-z]+)-compatible microchip", line):
            floor.append(Item("chip", element))
        for element in re.findall(r"([a-z]+) generator", line):
            floor.append(Item("generator", element))
        floors.append(floor)
initial_state = State(0, floors)
print("Minimum number of steps (part 1): ", end="")
bfs(initial_state)
initial_state.floors[0].extend([Item("chip", "elerium"), Item("generator", "elerium"),
                                Item("chip", "dilithium"), Item("generator", "dilithium")])
print("Minimum number of steps (part 2): ", end="")
bfs(initial_state)
