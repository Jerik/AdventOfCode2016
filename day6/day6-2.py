message = ""
with open("input6.txt") as input_file:
    f = input_file.read().strip()
for i in range(f.find("\n")):
    letters = []
    for line in f.split("\n"):
        letters.append(line[i])
    sorted_letters = sorted(letters, key=lambda letter: letters.count(letter))
    message += sorted_letters[0]
print("Message: {0}".format(message))
