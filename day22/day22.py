import re
from itertools import combinations


class Node:

    def __init__(self, x, y, size, used, avail, percent_used):
        self.x = x
        self.y = y
        self.size = size
        self.used = used
        self.avail = avail
        self.percent_used = percent_used


nodes = []
viable_nodes = set()
with open("input22.txt") as input_file:
    for line in input_file:
        params = [int(num) for num in re.findall(r"[0-9]+", line)]
        if params:
            nodes.append(Node(*params))
viable_pairs = 0
for comb in combinations(nodes, 2):
    if comb[0].used <= comb[1].avail and comb[0].used != 0:
        viable_pairs += 1
        viable_nodes.add(comb[0])
        viable_nodes.add(comb[1])
    elif comb[1].used <= comb[0].avail and comb[1].used != 0:
        viable_pairs += 1
        viable_nodes.add(comb[0])
        viable_nodes.add(comb[1])
print("Number of viable pairs: {0}".format(viable_pairs))
print("\nGrid: ")
for i in range(max([node.y for node in nodes]) + 1):
    for j in range(max([node.x for node in nodes]) + 1):
        if i == 0 and j == 0:
            print("G", end=" ")
        elif i == 0 and j == max([node.x for node in nodes]):
            print("D", end=" ")
        else:
            for node in nodes:
                if node.x == j and node.y == i:
                    if node.used == 0:
                        print("_", end=" ")
                    elif node in viable_nodes:
                        print(".", end=" ")
                    else:
                        print("#", end=" ")
                    break
    print()
