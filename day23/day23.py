def get_number(string):
    try:
        return int(string)
    except ValueError:
        return registers[string]


def get_register_a():
    with open("input23.txt") as input_file:
        instructions = [line.strip().split() for line in input_file]
    start = 0
    while start < len(instructions):
        for i, instr in enumerate(instructions[start:]):
            pos = start + i
            five_instrs = [instructions[j] for j in range(pos, pos + 5)]
            if [one_instr[0] for one_instr in five_instrs] == ["inc", "dec", "jnz", "dec", "jnz"]:
                registers[five_instrs[0][1]] += registers[five_instrs[1][1]] * registers[five_instrs[3][1]]
                start = pos + 5
                break
            command = instr[0]
            x = instr[1]
            if len(instr) == 3:
                y = instr[2]
            if command == "cpy":
                registers[y] = get_number(x)
            elif command == "inc":
                registers[x] += 1
            elif command == "dec":
                registers[x] -= 1
            elif command == "jnz":
                if get_number(x) != 0:
                    start = pos + get_number(y)
                else:
                    start = pos + 1
                break
            elif command == "tgl":
                index = pos + get_number(x)
                if index < len(instructions):
                    if len(instructions[index]) < 3:
                        if instructions[index][0] == "inc":
                            instructions[index][0] = "dec"
                        else:
                            instructions[index][0] = "inc"
                    else:
                        if instructions[index][0] == "jnz":
                            instructions[index][0] = "cpy"
                        else:
                            instructions[index][0] = "jnz"
        else:
            break
    return registers["a"]


registers = {"a": 7, "b": 0, "c": 0, "d": 0}
print("Value of register a (part 1): {0}".format(get_register_a()))
registers["a"] = 12
print("Value of register a (part 2): {0}".format(get_register_a()))
