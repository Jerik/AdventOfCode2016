import re


def decompress(string):
    length = 0
    marker = re.search(r"\((?P<num_of_ch>[0-9]+)x(?P<repeats>[0-9]+)\)", string)
    if marker:
        num_of_ch = int(marker.group("num_of_ch"))
        repeats = int(marker.group("repeats"))
        length += marker.start()
        length += decompress(string[marker.end():marker.end() + num_of_ch]) * repeats
        length += decompress(string[marker.end() + num_of_ch:])
    else:
        length = len(string)
    return length

with open("input9.txt") as input_file:
    f = input_file.read().strip()
print("Decompressed length: {0}".format(decompress(f)))
