import re

with open("input9.txt") as input_file:
    f = input_file.read().strip()
ch_groups = []
while True:
    marker = re.search(r"\((?P<num_of_ch>[0-9]+)x(?P<repeats>[0-9]+)\)", f)
    if marker:
        num_of_ch = int(marker.group("num_of_ch"))
        repeats = int(marker.group("repeats"))
        ch_group = f[:marker.start()]
        repeated_ch = f[marker.end():marker.end() + num_of_ch]
        for i in range(repeats):
            ch_group += repeated_ch
        ch_groups.append(ch_group)
        f = f[marker.end() + num_of_ch:]
    else:
        ch_groups.append(f)
        break
print("Decompressed length: {0}".format(len("".join(ch_groups))))
