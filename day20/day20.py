lower_borders = []
upper_borders = []
with open("input20.txt") as input_file:
    for line in input_file:
        if "-" in line:
            lower_borders.append(int(line.strip().split("-")[0]))
            upper_borders.append(int(line.strip().split("-")[1]))
lower_borders.sort()
upper_borders.sort()
limit = 4294967295
allowed_ips = 0
lowest_ip = None
for i in range(lower_borders[0]):
    if lowest_ip is None:
        lowest_ip = 0
    allowed_ips += 1
for i, ub in enumerate(upper_borders[:-1]):
    if ub < limit:
        for j in range(ub + 1, lower_borders[i+1]):
            if lowest_ip is None:
                lowest_ip = ub + 1
            allowed_ips += 1
for i in range(upper_borders[-1], limit):
    if lowest_ip is None:
        lowest_ip = upper_borders[-1] + 1
    allowed_ips += 1
print("Lowest allowed IP: {0}".format(lowest_ip))
print("Number of allowed IPs: {0}".format(allowed_ips))
