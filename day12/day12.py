def get_number(string):
    try:
        return int(string)
    except ValueError:
        return registers[string]


def get_register_a():
    start = 0
    while start < len(instructions):
        for i, instr in enumerate(instructions[start:]):
            pos = start + i
            command = instr[0]
            x = instr[1]
            if len(instr) == 3:
                y = instr[2]
            if command == "cpy":
                registers[y] = get_number(x)
            elif command == "inc":
                registers[x] += 1
            elif command == "dec":
                registers[x] -= 1
            elif command == "jnz":
                if get_number(x) != 0:
                    start = pos + get_number(y)
                else:
                    start = pos + 1
                break
        else:
            break
    return registers["a"]


with open("input12.txt") as input_file:
    instructions = [line.strip().split() for line in input_file]
registers = {"a": 0, "b": 0, "c": 0, "d": 0}
print("Value of register a (part 1): {0}".format(get_register_a()))
registers["c"] = 1
print("Value of register a (part 2): {0}".format(get_register_a()))
