def get_tile(tiles_above):
    if tiles_above[0] != tiles_above[2]:
        return "^"
    else:
        return "."


def get_next_row(row):
    next_row = get_tile("." + row[:2])
    for i in range(1, len(row) - 1):
        next_row += get_tile(row[i-1:i+2])
    next_row += get_tile(row[-2:] + ".")
    return next_row


def get_safe_tiles(first_row, num_of_rows):
    safe_tiles = 0
    current_row = first_row
    for i in range(num_of_rows):
        safe_tiles += current_row.count(".")
        current_row = get_next_row(current_row)
    return safe_tiles


with open("input18.txt") as input_file:
    f = input_file.read().strip()
print("Number of safe tiles (part 1): {0}".format(get_safe_tiles(f, 40)))
print("Number of safe tiles (part 2): {0}".format(get_safe_tiles(f, 400000)))
