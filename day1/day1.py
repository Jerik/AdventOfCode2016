def check_pos(axis_name, axis_value, num_of_steps, step):
    for i in range(1, num_of_steps+1):
        temp = axis_value + i * step
        if axis_name == "x":
            current_pos = (temp, y)
        elif axis_name == "y":
            current_pos = (x, temp)
        if current_pos not in visited_locs:
            visited_locs.append(current_pos)
        else:
            return abs(current_pos[0]) + abs(current_pos[1])
    return None


with open("input1.txt") as input_file:
    instructions = input_file.read().strip().split(", ")
x = 0
y = 0
direction = 0
visited_locs = [(0, 0)]
distance_part2 = None
for instr in instructions:
    rotation = instr[0]
    num_of_steps = int(instr[1:])
    if rotation == "R":
        direction = (direction + 90) % 360
    elif rotation == "L":
        direction = (direction - 90) % 360
    if direction == 0:
        if not distance_part2:
            distance_part2 = check_pos("y", y, num_of_steps, 1)
        y += num_of_steps
    elif direction == 90:
        if not distance_part2:
            distance_part2 = check_pos("x", x, num_of_steps, 1)
        x += num_of_steps
    elif direction == 180:
        if not distance_part2:
            distance_part2 = check_pos("y", y, num_of_steps, -1)
        y -= num_of_steps
    elif direction == 270:
        if not distance_part2:
            distance_part2 = check_pos("x", x, num_of_steps, -1)
        x -= num_of_steps
print("Distance (part 1): {0}".format(abs(x) + abs(y)))
print("Distance (part 2): {0}".format(distance_part2))
