def check_number(num):
    if num > 2:
        return 2
    elif num < 0:
        return 0
    else:
        return num

fields = [[7, 8, 9], [4, 5, 6], [1, 2, 3]]
row = 1
col = 1
print("Code: ", end="")
with open("input2.txt") as input_file:
    for line in input_file:
        for ch in line:
            if ch == "U":
                row = check_number(row+1)
            elif ch == "D":
                row = check_number(row-1)
            elif ch == "R":
                col = check_number(col+1)
            elif ch == "L":
                col = check_number(col-1)
        print(fields[row][col], end="")
