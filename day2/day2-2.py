def check_number(num):
    if num > 4:
        return 4
    elif num < 0:
        return 0
    else:
        return num

fields = [[0, 0, "D", 0, 0], [0, "A", "B", "C", 0], [5, 6, 7, 8, 9], [0, 2, 3, 4, 0], [0, 0, 1, 0, 0]]
row = 2
col = 0
print("Code: ", end="")
with open("input2.txt") as input_file:
    for line in input_file:
        for ch in line:
            if ch == "U":
                temp = check_number(row+1)
                if fields[temp][col]:
                    row = temp
            elif ch == "D":
                temp = check_number(row-1)
                if fields[temp][col]:
                    row = temp
            elif ch == "R":
                temp = check_number(col+1)
                if fields[row][temp]:
                    col = temp
            elif ch == "L":
                temp = check_number(col-1)
                if fields[row][temp]:
                    col = temp
        print(fields[row][col], end="")
