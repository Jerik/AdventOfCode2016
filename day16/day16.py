def get_dragon_curve(init, length):
    a = init
    while len(a) < length:
        b = ""
        for ch in reversed(a):
            b += "0" if ch == "1" else "1"
        a = "{0}{1}{2}".format(a, "0", b)
    return a[:length]


def get_checksum(curve):
    checksum = curve
    while len(checksum) % 2 == 0:
        pairs = [(ch, checksum[i+1]) for i, ch in enumerate(checksum) if i % 2 == 0]
        checksum = ""
        for pair in pairs:
            checksum += "1" if pair[0] == pair[1] else "0"
    return checksum

dragon_curve1 = get_dragon_curve("01110110101001000", 272)
print("Checksum (part 1): {0}".format(get_checksum(dragon_curve1)))
dragon_curve2 = get_dragon_curve("01110110101001000", 35651584)
print("Checksum (part 2): {0}".format(get_checksum(dragon_curve2)))
