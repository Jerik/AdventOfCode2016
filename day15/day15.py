import re


def get_time():
    press_time = 0
    while True:
        if all([disc.get_position(press_time) == 0 for disc in discs]):
            return press_time
        press_time += 1


class Disc:

    def __init__(self, disc_id, num_of_pos, starting_pos):
        self.id = disc_id
        self.num_of_pos = num_of_pos
        self.starting_pos = starting_pos

    def get_position(self, press_time):
        return (self.starting_pos + press_time + self.id) % self.num_of_pos


discs = []
with open("input15.txt") as input_file:
    for line in input_file:
        args = [int(num) for num in re.findall(r"[0-9]+", line)]
        del args[2]
        discs.append(Disc(*args))
print("Press time to get a capsule (part 1): {0}".format(get_time()))
discs.append(Disc(7, 11, 0))
print("Press time to get a capsule (part 2): {0}".format(get_time()))
