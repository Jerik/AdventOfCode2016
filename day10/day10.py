import re
from collections import defaultdict


def convert_to_int(string):
    try:
        return int(string)
    except ValueError:
        return string


def get_bot(bot_id):
    for bot in bots:
        if bot.id == bot_id:
            return bot
    else:
        bots.append(Bot(bot_id))
        return bots[-1]


def move_values(bot_id, low_target_type, low_target_id, high_target_type, high_target_id):
    bot = get_bot(bot_id)
    if bot.value1 != 0 and bot.value2 != 0:
        if low_target_type == "bot":
            receive_bot = get_bot(low_target_id)
            receive_bot.add_value(bot.get_lower_value())
        elif low_target_type == "output":
            outputs[low_target_id].append(bot.get_lower_value())
        if high_target_type == "bot":
            receive_bot = get_bot(high_target_id)
            receive_bot.add_value(bot.get_higher_value())
        elif high_target_type == "output":
            outputs[high_target_id].append(bot.get_higher_value())
        bot.value1 = 0
        bot.value2 = 0
        return True


class Bot:

    def __init__(self, bot_id):
        self.id = bot_id
        self.value1 = 0
        self.value2 = 0

    def get_lower_value(self):
        if self.value1 < self.value2:
            return self.value1
        else:
            return self.value2

    def get_higher_value(self):
        if self.value1 > self.value2:
            return self.value1
        else:
            return self.value2

    def add_value(self, value):
        if self.value1 == 0:
            self.value1 = value
        elif self.value2 == 0:
            self.value2 = value

    def check_values(self, values):
        if self.value1 in values and self.value2 in values:
            return True


bots = []
outputs = defaultdict(list)
bot_found = False
with open("input10.txt") as input_file:
    initialisations = [line.strip() for line in input_file if line.startswith("value")]
    input_file.seek(0)
    instructions = [line.strip() for line in input_file if line.startswith("bot")]
for init in initialisations:
    init_data = re.match(r"value (?P<value>[0-9]+) goes to bot (?P<bot_id>[0-9]+)", init)
    bot = get_bot(int(init_data.group("bot_id")))
    bot.add_value(int(init_data.group("value")))
while len(instructions) > 0:
    for instr in instructions:
        if bot_found is False:
            for bot in bots:
                if bot.check_values((61, 17)):
                    print("Number of the bot that compares chips 61 and 17: {0}".format(bot.id))
                    bot_found = True
                    break
        instr_data = re.match(r"bot ([0-9]+) gives low to ([a-z]+) ([0-9]+) and high to ([a-z]+) ([0-9]+)", instr)
        if instr_data:
            args = [convert_to_int(group) for group in instr_data.groups()]
            if move_values(*args):
                instructions.remove(instr)
print("Product of chip values in outputs 0, 1 and 2: {0}".format(outputs[0][0]*outputs[1][0]*outputs[2][0]))
