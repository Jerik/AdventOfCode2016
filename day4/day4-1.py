import re

id_sum = 0
pattern = r"(?P<name>([a-z]+-?)+)-(?P<id>[0-9]+)\[(?P<checksum>[a-z]{5})\]"
with open("input4.txt") as input_file:
    for line in input_file:
        room_data = re.match(pattern, line)
        if room_data:
            letters = [letter for letter in room_data.group("name") if letter != "-"]
            letters.sort()
            needed_checksum = ""
            for letter in sorted(letters, key=lambda letter: letters.count(letter), reverse=True):
                if len(needed_checksum) == 5:
                    break
                if letter not in needed_checksum:
                    needed_checksum += letter
            if room_data.group("checksum") == needed_checksum:
                id_sum += int(room_data.group("id"))
print("Sum of sector IDs: {0}".format(id_sum))
