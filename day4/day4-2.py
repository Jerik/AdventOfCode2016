import re
import string

alphabet = [letter for letter in string.ascii_lowercase]
pattern = r"(?P<name>([a-z]+-?)+)-(?P<id>[0-9]+)\[(?P<checksum>[a-z]{5})\]"
with open("input4.txt") as input_file:
    for line in input_file:
        room_data = re.match(pattern, line)
        if room_data:
            room_name = ""
            for letter in room_data.group("name"):
                if letter == "-":
                    room_name += " "
                else:
                    room_name += alphabet[(alphabet.index(letter) + int(room_data.group("id"))) % 26]
            if "north" in room_name:
                print("Sector ID of the northpole storage room: {0}".format(room_data.group("id")))
                break
