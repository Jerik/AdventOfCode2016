import hashlib
from collections import deque


def bfs(init):
    longest_path_length = 0
    found_shortest_path = False
    queue = deque([init])
    while len(queue) > 0:
        current = queue.popleft()
        for child_state in current.get_child_states():
            if all([0 <= coord <= 3 for coord in child_state.room]):
                if child_state.room == (3, 3):
                    if found_shortest_path is False:
                        print("Shortest path: {0}".format(child_state.path))
                        found_shortest_path = True
                    longest_path_length = len(child_state.path)
                else:
                    queue.append(child_state)
    print("Length of longest path: {0}".format(longest_path_length))


class State:

    def __init__(self, room, path):
        self.room = room
        self.path = path

    def get_child_states(self):
        child_states = []
        passcode = "hhhxzeay"
        string = passcode + self.path
        hashed_str = hashlib.md5()
        hashed_str.update(string.encode(encoding="utf8"))
        for i, ch in enumerate(hashed_str.hexdigest()[:4]):
            if ch in "bcdef":
                if i == 0:
                    child_states.append(State((self.room[0], self.room[1] - 1), self.path + "U"))
                elif i == 1:
                    child_states.append(State((self.room[0], self.room[1] + 1), self.path + "D"))
                elif i == 2:
                    child_states.append(State((self.room[0] - 1, self.room[1]), self.path + "L"))
                elif i == 3:
                    child_states.append(State((self.room[0] + 1, self.room[1]), self.path + "R"))
        return child_states


initial_state = State((0, 0), "")
bfs(initial_state)
