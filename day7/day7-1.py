import re


def check_abba(sequence):
    abba_pattern = r"([a-z])([a-z])(\2)(\1)"
    abba = re.search(abba_pattern, sequence)
    if abba:
        if abba.group(1) != abba.group(2):
            return True
    return False

tls_ips = 0
with open("input7.txt") as input_file:
    for line in input_file:
        sequences = re.split(r"\[|\]", line)
        outside = []
        inside = []
        for i, seq in enumerate(sequences):
            if i % 2 == 0:
                outside.append(check_abba(seq))
            else:
                inside.append(check_abba(seq))
        if any(outside) and not any(inside):
            tls_ips += 1
print("Number of TLS-IPs: {0}".format(tls_ips))
