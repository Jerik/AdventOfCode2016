import re


def get_babs(sequence):
    babs = []
    aba_pattern = r"(?=([a-z])([a-z])(\1))"
    abas = re.findall(aba_pattern, sequence)
    if abas:
        for aba in abas:
            if aba[0] != aba[1]:
                babs.append(aba[1] + aba[0] + aba[1])
    return babs


def check_seq_for_babs(sequence):
    for bab in possible_babs:
        if bab in sequence:
            return True

ssl_ips = 0
with open("input7.txt") as input_file:
    for line in input_file:
        sequences = re.split(r"\[|\]", line)
        possible_babs = []
        for seq in filter(lambda x: sequences.index(x) % 2 == 0, sequences):
            possible_babs += get_babs(seq)
        for seq in filter(lambda x: sequences.index(x) % 2 != 0, sequences):
            if check_seq_for_babs(seq):
                ssl_ips += 1
                break
print("Number of SSL-IPs: {0}".format(ssl_ips))
