import re


class Screen:

    def __init__(self, cols, rows):
        self.pixels = [[False for i in range(cols)] for j in range(rows)]

    def display(self):
        for row in self.pixels:
            for pixel in row:
                if pixel is True:
                    print("#", end="")
                else:
                    print(".", end="")
            print()

    def rect(self, cols, rows):
        for i in range(cols):
            for j in range(rows):
                self.pixels[j][i] = True

    def rotate_col(self, col, num):
        values = [row[col] for row in self.pixels]
        for i in range(num):
            values.insert(0, values.pop(-1))
        for i, row in enumerate(self.pixels):
            row[col] = values[i]

    def rotate_row(self, row, num):
        for i in range(num):
            self.pixels[row].insert(0, self.pixels[row].pop(-1))

    def get_lit_pixels(self):
        lit_pixels = 0
        for row in self.pixels:
            for pixel in row:
                if pixel is True:
                    lit_pixels += 1
        print("Number of lit pixels: {0}".format(lit_pixels))


lcd = Screen(50, 6)
with open("input8.txt") as input_file:
    for line in input_file:
        args = [int(num) for num in re.findall(r"[0-9]+", line)]
        if line.startswith("rect"):
            lcd.rect(*args)
        elif line.startswith("rotate column"):
            lcd.rotate_col(*args)
        elif line.startswith("rotate row"):
            lcd.rotate_row(*args)
lcd.get_lit_pixels()
print("\nDisplay:")
lcd.display()
