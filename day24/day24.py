from collections import deque, namedtuple, OrderedDict
from itertools import combinations, permutations


def bfs(start, goal, fields):
    visited_field_to_distance = {start: 0}
    queue = deque([start])
    while len(queue) > 0:
        current = queue.popleft()
        for adj_field in [field for field in fields if abs(current.x - field.x) == 1 and abs(current.y - field.y) == 0
                          or abs(current.x - field.x) == 0 and abs(current.y - field.y) == 1]:
            if adj_field not in visited_field_to_distance:
                visited_field_to_distance[adj_field] = visited_field_to_distance[current] + 1
                queue.append(adj_field)
                if adj_field == goal:
                    return visited_field_to_distance[adj_field]


Field = namedtuple("Field", ["x", "y"])

open_fields = []
num_to_field = OrderedDict()
starting_field = None
with open("input24.txt") as input_file:
    for i, line in enumerate(input_file):
        for j, ch in enumerate(line.strip()):
            if ch != "#":
                open_fields.append(Field(j, i))
                if ch != ".":
                    if ch == "0":
                        starting_field = Field(j, i)
                    else:
                        num_to_field[int(ch)] = (Field(j, i))
distances_from_start = [bfs(starting_field, num_to_field[num], open_fields) for num in sorted(num_to_field)]
other_distances = [[None for i in range(len(num_to_field))] for j in range(len(num_to_field))]
for comb in combinations(sorted(num_to_field), 2):
    distance = bfs(num_to_field[comb[0]], num_to_field[comb[1]], open_fields)
    other_distances[comb[0]-1][comb[1]-1] = distance
    other_distances[comb[1]-1][comb[0]-1] = distance
path_lengths_part1 = []
path_lengths_part2 = []
for perm in permutations(num_to_field):
    path_length = distances_from_start[perm[0]-1]
    for i, num in enumerate(perm[1:]):
        path_length += other_distances[num-1][perm[i]-1]
    path_lengths_part1.append(path_length)
    path_length += distances_from_start[perm[-1]-1]
    path_lengths_part2.append(path_length)
print("Fewest number of steps (part 1): {0}".format(min(path_lengths_part1)))
print("Fewest number of steps (part 2): {0}".format(min(path_lengths_part2)))
