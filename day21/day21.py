from itertools import permutations


def convert_to_int(string):
    try:
        return int(string)
    except ValueError:
        return string


def swap_pos(pw, x, y):
    li = x if x < y else y
    ui = x if x > y else y
    return pw[:li] + pw[ui] + pw[li+1:ui] + pw[li] + pw[ui+1:]


def swap_letter(pw, x, y):
    return swap_pos(pw, list(pw).index(x), list(pw).index(y))


def rotate_rl(pw, di, x):
    if di == "R":
        for i in range(x):
            pw = pw[-1] + pw[:-1]
    elif di == "L":
        for i in range(x):
            pw = pw[1:] + pw[0]
    return pw


def rotate_by_pos(pw, x):
    rotations = list(pw).index(x)
    if rotations >= 4:
        rotations += 1
    return rotate_rl(pw, "R", rotations + 1)


def reverse(pw, x, y):
    return pw[:x] + "".join(reversed(pw[x:y+1])) + pw[y+1:]


def move(pw, x, y):
    return pw.replace(pw[x], "")[:y] + pw[x] + pw.replace(pw[x], "")[y:]


with open("input21.txt") as input_file:
    operations = input_file.read().split("\n")
for perm in permutations("abcdefgh"):
    password = "".join(perm)
    for op in operations:
        op_parts = [convert_to_int(ch_group) for ch_group in op.strip().split()]
        if op.startswith("swap position"):
            password = swap_pos(password, op_parts[2], op_parts[5])
        elif op.startswith("swap letter"):
            password = swap_letter(password, op_parts[2], op_parts[5])
        elif op.startswith("rotate left"):
            password = rotate_rl(password, "L", op_parts[2])
        elif op.startswith("rotate right"):
            password = rotate_rl(password, "R", op_parts[2])
        elif op.startswith("rotate based"):
            password = rotate_by_pos(password, op_parts[6])
        elif op.startswith("reverse"):
            password = reverse(password, op_parts[2], op_parts[4])
        elif op.startswith("move"):
            password = move(password, op_parts[2], op_parts[5])
    if "".join(perm) == "abcdefgh":
        print("Password (scrambled \"abcdefgh\"): {0}".format(password))
    if password == "fbgdceah":
        print("Original (unscrambled \"fbgdceah\"): {0}".format("".join(perm)))
        break
