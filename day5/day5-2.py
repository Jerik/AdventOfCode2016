import hashlib


def check_pos(position):
    try:
        if 0 <= int(position) <= 7 and password[int(position)] == "":
            return True
    except ValueError:
        return False

door_id = "ffykfhsq"
index = 0
password = ["" for i in range(8)]
while "" in password:
    string = door_id + str(index)
    hashed_str = hashlib.md5()
    hashed_str.update(string.encode(encoding="utf8"))
    if hashed_str.hexdigest().startswith("00000"):
        pos = hashed_str.hexdigest()[5]
        ch = hashed_str.hexdigest()[6]
        if check_pos(pos):
            password[int(pos)] = ch
    index += 1
print("Password: {0}".format("".join(password)))
