import hashlib

door_id = "ffykfhsq"
index = 0
password = ""
while len(password) < 8:
    string = door_id + str(index)
    hashed_str = hashlib.md5()
    hashed_str.update(string.encode(encoding="utf8"))
    if hashed_str.hexdigest().startswith("00000"):
        password += hashed_str.hexdigest()[5]
    index += 1
print("Password: {0}".format(password))
