import hashlib
import re
from collections import OrderedDict

salt = "ahsbgdzn"
index = 0
key_indices = []
index_to_triplet = OrderedDict()
while len(key_indices) < 64:
    string = salt + str(index)
    hashed_str = hashlib.md5()
    hashed_str.update(string.encode(encoding="utf8"))
    triplet = re.search(r"([a-f0-9])\1\1", hashed_str.hexdigest())
    quintuplet = re.search(r"([a-f0-9])\1\1\1\1", hashed_str.hexdigest())
    if quintuplet:
        for i in index_to_triplet:
            if index_to_triplet[i] == quintuplet.group(1) and index - 1000 <= i:
                key_indices.append(i)
                if len(key_indices) == 64:
                    print("Index producing the 64th key: {0}".format(key_indices[-1]))
                    break
    if triplet:
        index_to_triplet[index] = triplet.group(1)
    index += 1
